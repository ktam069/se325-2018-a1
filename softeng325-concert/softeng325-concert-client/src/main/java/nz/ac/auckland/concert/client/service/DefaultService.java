package nz.ac.auckland.concert.client.service;

import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import java.awt.Image;
import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nz.ac.auckland.concert.common.dto.BookingDTO;
import nz.ac.auckland.concert.common.dto.ConcertDTO;
import nz.ac.auckland.concert.common.dto.CreditCardDTO;
import nz.ac.auckland.concert.common.dto.PerformerDTO;
import nz.ac.auckland.concert.common.dto.ReservationDTO;
import nz.ac.auckland.concert.common.dto.ReservationRequestDTO;
import nz.ac.auckland.concert.common.dto.UserDTO;
import nz.ac.auckland.concert.common.message.Messages;
import nz.ac.auckland.concert.service.domain.Config;
import nz.ac.auckland.concert.service.domain.User;
import nz.ac.auckland.concert.service.services.PersistenceManager;

public class DefaultService implements ConcertService {
	
	private static Logger _logger = LoggerFactory.getLogger(DefaultService.class);
	private static String WEB_SERVICE_URI = "http://localhost:10000/services/concerts";

	private Cookie cookie = null;
	
//	private static List<ConcertDTO> _concertDTOs = new ArrayList<ConcertDTO>();
//	private static List<String> _concertUris = new ArrayList<String>();

	public DefaultService() {
		// reinitialise new tables created during test cases
		EntityManager em = null;
		try {
			em = PersistenceManager.instance().createEntityManager();
			em.getTransaction().begin();
			em.createQuery("delete from User").executeUpdate();
			em.createQuery("delete from Seat").executeUpdate();
			em.createQuery("delete from Reservation").executeUpdate();
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}
	
	@Override
	public Set<ConcertDTO> getConcerts() throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI).request().accept(MediaType.APPLICATION_XML);
			response = builder.get();
			Set<ConcertDTO> cDTOs = response.readEntity(new GenericType<Set<ConcertDTO>>() {});
						
			return cDTOs;
		} catch (Exception e) {
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public Set<PerformerDTO> getPerformers() throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI+"/performers").request().accept(MediaType.APPLICATION_XML);
			response = builder.get();
			Set<PerformerDTO> pDTOs = response.readEntity(new GenericType<Set<PerformerDTO>>() {});
			
			return pDTOs;
		} catch (Exception e) {
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public UserDTO createUser(UserDTO newUser) throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI+"/user").request();
			response = builder.post(Entity.entity(newUser, MediaType.APPLICATION_XML));			
			
			if (response.getStatus()==Response.Status.BAD_REQUEST.getStatusCode()) {
				throw new ServiceException(Messages.CREATE_USER_WITH_MISSING_FIELDS);
			} else if (response.getStatus()==Response.Status.CONFLICT.getStatusCode()) {
				throw new ServiceException(Messages.CREATE_USER_WITH_NON_UNIQUE_NAME);
			}
			
			processCookieFromResponse(response);			// process cookie from response if exists
						
			return newUser;
		} catch (ServiceException e) {
			_logger.info("\n--se error-- \n"+e.toString());
			throw e;
		} catch (Exception e) {
//			e.printStackTrace();
			_logger.info("\n--error-- \n"+e.toString());
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public UserDTO authenticateUser(UserDTO user) throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI+"/authen").request();
			response = builder.post(Entity.entity(user, MediaType.APPLICATION_XML));			
			
			if (response.getStatus()==Response.Status.BAD_REQUEST.getStatusCode()) {
				throw new ServiceException(Messages.AUTHENTICATE_USER_WITH_MISSING_FIELDS);
			} else if (response.getStatus()==Response.Status.NOT_FOUND.getStatusCode()) {
				throw new ServiceException(Messages.AUTHENTICATE_NON_EXISTENT_USER);
			} else if (response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()) {
				throw new ServiceException(Messages.AUTHENTICATE_USER_WITH_ILLEGAL_PASSWORD);
			}
			
			UserDTO uDTO = response.readEntity(UserDTO.class);
			processCookieFromResponse(response);			// process cookie from response if exists
			return uDTO;
		} catch (ServiceException e) {
			_logger.info("\n--se error in authen-- \n"+e.toString());
			throw e;
		} catch (Exception e) {
//			e.printStackTrace();
			_logger.info("\n--error in authen-- \n"+e.toString());
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public Image getImageForPerformer(PerformerDTO performer) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReservationDTO reserveSeats(ReservationRequestDTO reservationRequest) throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI+"/reserve").request();
			addCookieToInvocation(builder);
			response = builder.post(Entity.entity(reservationRequest, MediaType.APPLICATION_XML));			
			
			if (response.getStatus()==Response.Status.BAD_REQUEST.getStatusCode()) {
				throw new ServiceException(Messages.RESERVATION_REQUEST_WITH_MISSING_FIELDS);
			} else if (response.getStatus()==Response.Status.NOT_FOUND.getStatusCode()) {
				throw new ServiceException(Messages.CONCERT_NOT_SCHEDULED_ON_RESERVATION_DATE);
			} else if (response.getStatus()==Response.Status.CONFLICT.getStatusCode()) {
				throw new ServiceException(Messages.INSUFFICIENT_SEATS_AVAILABLE_FOR_RESERVATION);
			} else if (response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()) {
				throw new ServiceException(Messages.UNAUTHENTICATED_REQUEST);
			} else if (response.getStatus()==Response.Status.NOT_ACCEPTABLE.getStatusCode()) {
				throw new ServiceException(Messages.BAD_AUTHENTICATON_TOKEN);
			}
			
			ReservationDTO rDTO = response.readEntity(ReservationDTO.class);
			return rDTO;
		} catch (ServiceException e) {
			_logger.info("\n--se error in reserve-- \n"+e.toString());
			throw e;
		} catch (Exception e) {
//			e.printStackTrace();
			_logger.info("\n--error in reserve-- \n"+e.toString());
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public void confirmReservation(ReservationDTO reservation) throws ServiceException {
		Client _client = ClientBuilder.newClient();
		Response response = null;
		try {
			
			Builder builder = _client.target(WEB_SERVICE_URI+"/confirm").request();
			addCookieToInvocation(builder);
			response = builder.post(Entity.entity(reservation, MediaType.APPLICATION_XML));			
			
			if (response.getStatus()==Response.Status.BAD_REQUEST.getStatusCode()) {
				throw new ServiceException(Messages.RESERVATION_REQUEST_WITH_MISSING_FIELDS);
			} else if (response.getStatus()==Response.Status.NOT_FOUND.getStatusCode()) {
				throw new ServiceException(Messages.CONCERT_NOT_SCHEDULED_ON_RESERVATION_DATE);
			} else if (response.getStatus()==Response.Status.CONFLICT.getStatusCode()) {
				throw new ServiceException(Messages.INSUFFICIENT_SEATS_AVAILABLE_FOR_RESERVATION);
			} else if (response.getStatus()==Response.Status.UNAUTHORIZED.getStatusCode()) {
				throw new ServiceException(Messages.UNAUTHENTICATED_REQUEST);
			} else if (response.getStatus()==Response.Status.NOT_ACCEPTABLE.getStatusCode()) {
				throw new ServiceException(Messages.BAD_AUTHENTICATON_TOKEN);
			}
			
			return;
		} catch (ServiceException e) {
			_logger.info("\n--se error in confirm-- \n"+e.toString());
			throw e;
		} catch (Exception e) {
//			e.printStackTrace();
			_logger.info("\n--error in confirm-- \n"+e.toString());
			throw new ServiceException(Messages.SERVICE_COMMUNICATION_ERROR);
		} finally {
			if (response!=null) response.close();
			_client.close();
		}
	}

	@Override
	public void registerCreditCard(CreditCardDTO creditCard) throws ServiceException {
		// TODO Auto-generated method stub
		
		return;
	}

	@Override
	public Set<BookingDTO> getBookings() throws ServiceException {
		// TODO Auto-generated method stub
		
		Set<BookingDTO> bookings = new HashSet<BookingDTO>();
		
		
		
		return bookings;
	}
	
	private void addCookieToInvocation(Builder builder) {
		if(cookie != null) { builder.cookie(cookie); }
	}
	
	private void processCookieFromResponse(Response response) {
		Map<String, NewCookie> cookies = response.getCookies();
		
		if(cookies.containsKey(Config.CLIENT_COOKIE)) {
			Cookie c = cookies.get(Config.CLIENT_COOKIE).toCookie();
			cookie = c;
		}
	}
}
