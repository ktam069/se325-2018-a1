package nz.ac.auckland.concert.service.mappers;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import nz.ac.auckland.concert.common.dto.ConcertDTO;
import nz.ac.auckland.concert.service.domain.Concert;
import nz.ac.auckland.concert.service.domain.Performer;
import nz.ac.auckland.concert.service.services.PersistenceManager;

public class ConcertMapper {
	
	public static ConcertDTO toDTO(Concert concert) {
		return new ConcertDTO(
				concert.getId(),
				concert.getTitle(),
				concert.getDates(),
				concert.getTariff(),
				concert.getPerformerIds());
	}
	
	public static Concert toDM(ConcertDTO concertDTO) {
		return new Concert(
				concertDTO.getTitle(),
				concertDTO.getDates(),
				concertDTO.getTariff(),
				getPerformers( concertDTO.getPerformerIds() ));
	}
	
	private static Set<Performer> getPerformers(Set<Long> pIds) {
		Set<Performer> performers = new HashSet<Performer>();
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			for (Long id : pIds) {
				Performer p = em.find(Performer.class, id);
				performers.add(p);
			}
			em.getTransaction().commit();
			return performers;
			
		} finally {
			em.close();
		}
	}
}