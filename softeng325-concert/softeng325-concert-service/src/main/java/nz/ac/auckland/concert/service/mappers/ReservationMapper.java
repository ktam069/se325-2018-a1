package nz.ac.auckland.concert.service.mappers;

import java.util.HashSet;
import java.util.Set;

import nz.ac.auckland.concert.common.dto.ReservationDTO;
import nz.ac.auckland.concert.common.dto.SeatDTO;
import nz.ac.auckland.concert.service.domain.Reservation;
import nz.ac.auckland.concert.service.domain.Seat;

public class ReservationMapper {
	
	public static ReservationDTO toDTO(Reservation r) {
		return new ReservationDTO(
				r.getId(),
				r.getRequest(),
				getSeatDTOs( r.getSeats() ));
	}
	
	public static Reservation toDM(ReservationDTO rDTO) {
		return new Reservation(
				rDTO.getReservationRequest(),
				getSeats( rDTO.getSeats() ));
	}
	
	private static Set<Seat> getSeats(Set<SeatDTO> seatsDTO) {
		Set<Seat> seats = new HashSet<Seat>();
		for (SeatDTO s : seatsDTO) {
			seats.add(SeatMapper.toDM(s));
		}
		return seats;
	}

	private static Set<SeatDTO> getSeatDTOs(Set<Seat> seats) {
		Set<SeatDTO> seatsDTO = new HashSet<SeatDTO>();
		for (Seat s : seats) {
			seatsDTO.add(SeatMapper.toDTO(s));
		}
		return seatsDTO;
	}
	
}
