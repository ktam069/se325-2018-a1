package nz.ac.auckland.concert.service.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import nz.ac.auckland.concert.common.types.PriceBand;

@Entity
@Table(name="Concerts")
public class Concert {
	
	@Id
	@GeneratedValue
	@Column(nullable=false)
	private Long id;
	
	@Column(nullable=false)
	private String title;

	@ElementCollection
	@CollectionTable(name="Concert_Dates")
	private Set<LocalDateTime> dates;

	@ElementCollection
	@CollectionTable(name="Concert_Tarifs")
	@MapKeyEnumerated(EnumType.STRING)
	private Map<PriceBand, BigDecimal> tariff;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="Concert_Performer")
	private Set<Performer> performers;
	
	public Concert(String title, Set<LocalDateTime> dates, Map<PriceBand, BigDecimal> tariff,
			Set<Performer> performers) {
		super();
		this.title = title;
		this.dates = dates;
		this.tariff = tariff;
		this.performers = performers;
	}
	
	protected Concert() {}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<LocalDateTime> getDates() {
		return dates;
	}

	public void setDates(Set<LocalDateTime> dates) {
		this.dates = dates;
	}

	public Map<PriceBand, BigDecimal> getTariff() {
		return tariff;
	}

	public void setTariff(Map<PriceBand, BigDecimal> tariff) {
		this.tariff = tariff;
	}
	
	public Set<Performer> getPerformers() {
		return performers;
	}

	public void setPerformers(Set<Performer> performers) {
		this.performers = performers;
	}
	
	public Set<Long> getPerformerIds() {
		Set<Long> pIds = new HashSet<Long>();
		for (Performer p : performers) {
			pIds.add(p.getId());
		}
		return pIds;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Concert, id: ");
		buffer.append(id);
		buffer.append(", title: ");
		buffer.append(title);
		buffer.append(", date: ");
		buffer.append(dates.toString());
		buffer.append(", featuring: ");
		buffer.append(performers.toString());
		
		return buffer.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Concert))
            return false;
        if (obj == this)
            return true;

        Concert rhs = (Concert) obj;
        return new EqualsBuilder().
            append(title, rhs.getTitle()).
            append(dates, rhs.getDates()).
            isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). 
	            append(title).hashCode();
	}

}
