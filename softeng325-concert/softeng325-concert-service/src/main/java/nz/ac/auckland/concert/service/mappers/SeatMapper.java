package nz.ac.auckland.concert.service.mappers;

import nz.ac.auckland.concert.common.dto.SeatDTO;
import nz.ac.auckland.concert.service.domain.Seat;

public class SeatMapper {
	
	public static SeatDTO toDTO(Seat s) {
		return new SeatDTO(
				s.getRow(),
				s.getNumber());
	}
	
	public static Seat toDM(SeatDTO s) {
		return new Seat(
				s.getRow(),
				s.getNumber());
	}
	
}
