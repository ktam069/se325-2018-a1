package nz.ac.auckland.concert.service.domain;

/**
 * Class with shared configuration data for the client and Web service.
 *
 */
public class Config {
	/**
	 * Name of a cookie exchanged by clients and the Web service.
	 */
	public static final String CLIENT_COOKIE = "token";
	
	// Number of seats in each row from A to R
	public static final int[] SEAT_ROW_SIZES = {19, 20, 21, 21,
												21, 22, 23,
												22, 25, 25, 25, 26,
												26, 26, 26, 26};

	// Seat number counting from top left to bottom right, going across rows first
	public static final int[] SEAT_ROW_START_ID = {0, 19, 39, 60,
												81, 102, 124,
												147, 169, 194, 219, 244,
												270, 296, 322, 348};	// 374 seats total
	
	public static final int RESERVATION_EXPIRY_TIME_IN_SECONDS = 5;
}

