package nz.ac.auckland.concert.service.services;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import nz.ac.auckland.concert.common.dto.ConcertDTO;
import nz.ac.auckland.concert.common.dto.PerformerDTO;
import nz.ac.auckland.concert.common.dto.ReservationDTO;
import nz.ac.auckland.concert.common.dto.ReservationRequestDTO;
import nz.ac.auckland.concert.common.dto.SeatDTO;
import nz.ac.auckland.concert.common.dto.UserDTO;
import nz.ac.auckland.concert.common.types.PriceBand;
import nz.ac.auckland.concert.service.domain.Concert;
import nz.ac.auckland.concert.service.domain.Config;
import nz.ac.auckland.concert.service.domain.Performer;
import nz.ac.auckland.concert.service.domain.Reservation;
import nz.ac.auckland.concert.service.domain.Seat;
import nz.ac.auckland.concert.service.domain.User;
import nz.ac.auckland.concert.service.mappers.ConcertMapper;
import nz.ac.auckland.concert.service.mappers.PerformerMapper;
import nz.ac.auckland.concert.service.mappers.ReservationMapper;
import nz.ac.auckland.concert.service.mappers.SeatMapper;
import nz.ac.auckland.concert.service.mappers.UserMapper;
import nz.ac.auckland.concert.service.services.PersistenceManager;
import nz.ac.auckland.concert.service.util.TheatreUtility;

@Path("/concerts")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_XML)
public class ConcertResource {
		
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	public Response createConcert(ConcertDTO concertDTO) {
		Concert concert = ConcertMapper.toDM(concertDTO);
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(concert);
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
		
		return Response.created( URI.create("/concerts/" + concert.getId()) ).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public void updateConcert(ConcertDTO concertDTO) {
		Concert concert = ConcertMapper.toDM(concertDTO);
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			em.merge(concert);
			em.getTransaction().commit();
			
			// JAX-RS will add the default response code (204 No Content)
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getConcert(@PathParam("id")long id) {
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			Concert concert = em.find(Concert.class, id);
			em.getTransaction().commit();
			
			if (concert==null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
			ConcertDTO concertDTO = ConcertMapper.toDTO(concert);
			return Response.ok().entity(concertDTO).build();
			
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getConcerts() {
		Set<ConcertDTO> concertDTOs = new HashSet<ConcertDTO>();
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			List<Concert> concertsList = em.createQuery("select c from Concert c", Concert.class).getResultList();
			em.getTransaction().commit();
			
			if (concertsList.isEmpty()) { return Response.status(Response.Status.NO_CONTENT).build(); }
			
			for (Concert c : concertsList) {
				ConcertDTO cDTO = ConcertMapper.toDTO(c);
				concertDTOs.add(cDTO);
			}
			
			GenericEntity<Set<ConcertDTO>> ge = new GenericEntity<Set<ConcertDTO>>(concertDTOs) {};
			
			return Response.ok().entity(ge).build();
			
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}
	
	@GET
	@Path("/performers")
	@Produces(MediaType.APPLICATION_XML)
	public Response getPerformers() {
		Set<PerformerDTO> performerDTOs = new HashSet<PerformerDTO>();
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			List<Performer> performersList = em.createQuery("select p from Performer p", Performer.class).getResultList();
			em.getTransaction().commit();
			
			if (performersList.isEmpty()) { return Response.status(Response.Status.NO_CONTENT).build(); }
			
			for (Performer p : performersList) {
				PerformerDTO pDTO = PerformerMapper.toDTO(p);
				performerDTOs.add(pDTO);
			}
			
			GenericEntity<Set<PerformerDTO>> ge = new GenericEntity<Set<PerformerDTO>>(performerDTOs) {};
			
			return Response.ok().entity(ge).build();
			
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}

	@DELETE
	@Path("{id}")
	@Consumes(MediaType.APPLICATION_XML)
	public Response deleteConcert(@PathParam("id")long id) {
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			Concert concert = em.find(Concert.class, id);
			if (concert==null) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			em.remove(concert);
			em.getTransaction().commit();

			return Response.status(Response.Status.NO_CONTENT).build();
			
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_XML)
	public void deleteAllConcerts() {
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			em.createQuery("DELETE FROM Concert").executeUpdate();
			em.getTransaction().commit();

			// JAX-RS will add the default response code (204 No Content
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
	}
	
	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_XML)
	public Response createUser(UserDTO userDTO) {
		if (userDTO.getUsername()==null || userDTO.getPassword()==null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		User user = UserMapper.toDM(userDTO);
		
		NewCookie cookie = null;
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			if (em.find(User.class, user.getUsername()) != null) {
				return Response.status(Response.Status.CONFLICT).build();
			}
			em.persist(user);
			
			// give the user a newCookie if they don't have one
			cookie = makeCookie(user, user.getToken());		// returns null if token exists
			if (cookie!=null) em.persist(user);
			
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
		
		if (cookie==null) return Response.status(Response.Status.CREATED).entity(userDTO).build();
		else return Response.status(Response.Status.CREATED).entity(userDTO).cookie(cookie).build();
	}
	
	@POST
	@Path("/authen")
	@Consumes(MediaType.APPLICATION_XML)
	public Response authenUser(UserDTO userDTO) {
		if (userDTO.getUsername()==null || userDTO.getPassword()==null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		User matchedUser;
		NewCookie cookie = null;
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			matchedUser = em.find(User.class, userDTO.getUsername());
			if (matchedUser == null) {												// user does not exist
				return Response.status(Response.Status.NOT_FOUND).build();
			} else {																// user exists
				if ( !matchedUser.getPassword().equals(userDTO.getPassword()) ) {	// incorrect password
					return Response.status(Response.Status.UNAUTHORIZED).build();
				} else {															// correct credentials
					// give the user a newCookie if they don't have one
					cookie = makeCookie(matchedUser, matchedUser.getToken());		// returns null if token exists
					if (cookie!=null) em.persist(matchedUser);
				}
			}
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
		
		if (cookie==null) return Response.ok(UserMapper.toDTO(matchedUser)).build();
		else return Response.ok(UserMapper.toDTO(matchedUser)).cookie(cookie).build();
	}
	
	@POST
	@Path("/reserve")
	@Consumes(MediaType.APPLICATION_XML)
	public Response reserveSeat(ReservationRequestDTO rDTO, @CookieParam(Config.CLIENT_COOKIE) Cookie cookie) {
		if (cookie == null) { return Response.status(Response.Status.UNAUTHORIZED).build(); }
		
		String token = cookie.getValue();

		int numSeats = rDTO.getNumberOfSeats();
		PriceBand price = rDTO.getSeatType();
		Long cId = rDTO.getConcertId();
		LocalDateTime date = rDTO.getDate();
		
		if (numSeats==0 || price==null || cId==null || date==null) {		// missing fields
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		User matchedUser;
		Concert matchedConcert;
		ReservationDTO reservationDTO = null;
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			
			// authenticate user by checking token
			matchedUser = em.createQuery("select u from User u where u.token = :t", User.class)
						.setParameter("t", token).getSingleResult();
			if (matchedUser == null) {					// invalid token
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
			
			// check past reservations and remove any timed out reservations
//			em.createQuery("delete r from Reservation r where r._date < :d", Reservation.class)
//				.setParameter("d", LocalDateTime.now().minusSeconds(Config.RESERVATION_EXPIRY_TIME_IN_SECONDS))
//				.executeUpdate();
			
			// attempt to find the corresponding concert and date
			matchedConcert = em.createQuery("select c from Concert c where c.id = :i", Concert.class)
					.setParameter("i", cId).getSingleResult();
			if (matchedConcert==null || !matchedConcert.getDates().contains(date)) {	// invalid concert+date combo
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			
			// attempt to book seats for the particular date of the concert
			List<Seat> existingSeats = em.createQuery("select s from Seat s where s.c = :i", Seat.class)
					.setParameter("i", matchedConcert).getResultList();
			
			Set<SeatDTO> bookedSeats = new HashSet<SeatDTO>();
			Set<SeatDTO> availableSeats = new HashSet<SeatDTO>();

			if (existingSeats.isEmpty()) {		// prefill all seats if rows don't exist
				for (int i = 0; i < 374; i++) {
					Seat s = new Seat(new Integer(i+1), matchedConcert, date);
					em.persist(s);
				}
				em.flush();
				em.clear();
				
				availableSeats = TheatreUtility.findAvailableSeats(numSeats, price, bookedSeats);
			} else {			// seats already exist - check which ones aren't booked
				for (Seat s : existingSeats) {
					if (s.isBooked()) {
						bookedSeats.add(SeatMapper.toDTO(s));
					}
					
					availableSeats = TheatreUtility.findAvailableSeats(numSeats, price, bookedSeats);
				}
			}
			
			if (availableSeats.size() < numSeats) {			// not enough seats available
				return Response.status(Response.Status.CONFLICT).build();
			}
			
			List<SeatDTO> list = new ArrayList<SeatDTO>(availableSeats).subList(0, numSeats);
			Set<Seat> seatsToBook = new HashSet<Seat>();
			for (int i = 0; i < list.size(); i++) {			// generate the set of seats to be booked
				seatsToBook.add(SeatMapper.toDM( list.get(i) ));
			}
			
			// create and persist reservations
			Reservation res = new Reservation(rDTO, seatsToBook);
			em.persist(res);

			reservationDTO = ReservationMapper.toDTO(res);
			
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
		
//		GenericEntity<Set<SeatDTO>> ge = new GenericEntity<Set<SeatDTO>>(seatsToBook) {};
		return Response.ok(reservationDTO).build();
	}
	@POST
	@Path("/confirm")
	@Consumes(MediaType.APPLICATION_XML)
	public Response confirmRes(ReservationDTO rDTO, @CookieParam(Config.CLIENT_COOKIE) Cookie cookie) {
		// TODO debug reserving, and complete confirmations
		
		if (cookie == null) { return Response.status(Response.Status.UNAUTHORIZED).build(); }
		
		String token = cookie.getValue();

		Long resId = rDTO.getId();
		ReservationRequestDTO rrDTO = rDTO.getReservationRequest();
		Set<SeatDTO> seats = rDTO.getSeats();
		
		if (resId==null || rrDTO==null || seats==null) {		// missing fields
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		User matchedUser;
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			
			// authenticate user by checking token
			matchedUser = em.createQuery("select u from User u where u.token = :t", User.class)
						.setParameter("t", token).getSingleResult();
			if (matchedUser == null) {					// invalid token
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
			
			
			em.getTransaction().commit();
		} finally {
			if (em!=null && em.isOpen()) em.close();
		}
		
		return Response.ok().build();
	}
	
	private NewCookie makeCookie(User u, String token){
		NewCookie newCookie = null;
		
		String tokenValue = UUID.randomUUID().toString();
		if(token == null) {
			// create a cookie and update the user with the value of the cookie
			newCookie = new NewCookie(Config.CLIENT_COOKIE, tokenValue);
			u.setToken(tokenValue);
		}
		
		return newCookie;
	}
}
