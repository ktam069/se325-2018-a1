package nz.ac.auckland.concert.service.services;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import nz.ac.auckland.concert.service.domain.User;

@ApplicationPath("/services")
public class ConcertApplication extends Application {
	
	private Set<Class<?>> _classes = new HashSet<Class<?>>();
	
	public ConcertApplication() {
		// Register the Persistence manager class for JAXB.
		_classes.add(ConcertResource.class);
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		return _classes;
	}
}
