package nz.ac.auckland.concert.service.domain;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import nz.ac.auckland.concert.common.types.SeatNumber;
import nz.ac.auckland.concert.common.types.SeatRow;

@Entity
@Table(name="Seats")
public class Seat {
	
	@Id
	@GeneratedValue
	private Integer idPK;

	private Integer seatId;
	
	private SeatRow row;
	
	private SeatNumber number;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="c_id")
	private Concert c;

	private LocalDateTime date;
	
	private boolean booked;
	
	public Seat() {}
	
	public Seat(Integer seatId, Concert c, LocalDateTime date) {
		this.seatId = seatId;
		this.c = c;
		this.date = date;

		this.row = generateRow();
		this.number = generateNumber();
	}

	public Seat(SeatRow row, SeatNumber number, Concert c, LocalDateTime date) {
		this.row = row;
		this.number = number;
		this.c = c;
		this.date = date;

		this.seatId = generateId();
	}
	
	public Seat(SeatRow row, SeatNumber number) {
		this(row, number, null, null);
	}

	// Methods for converting between seatId and (row, number)
	// Not the most elegant; could be improved by attempting to use @IdClass and composite keys
	
	private Integer generateId() {
		int n = number.intValue();
		int r = row.ordinal();
		
		int id = Config.SEAT_ROW_START_ID[r] + n;
				
		return new Integer(id);
	}

	private SeatRow generateRow() {
		int id = seatId.intValue();
		
		int i = Config.SEAT_ROW_START_ID.length - 1;
		while(id > Config.SEAT_ROW_START_ID[i]) {
			i--;
		}
		
		int r = i;
				
		return SeatRow.values()[r];
	}
	
	private SeatNumber generateNumber() {
		int id = seatId.intValue();
		
		int i = Config.SEAT_ROW_START_ID.length - 1;
		while(id > Config.SEAT_ROW_START_ID[i]) {
			i--;
		}
		
		int n = id - Config.SEAT_ROW_START_ID[i];
				
		return new SeatNumber(n);
	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public SeatRow getRow() {
		return row;
	}

	public void setRow(SeatRow row) {
		this.row = row;
	}

	public SeatNumber getNumber() {
		return number;
	}

	public void setNumber(SeatNumber number) {
		this.number = number;
	}

	public Concert getConcert() {
		return c;
	}

	public void setConcert(Concert c) {
		this.c = c;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	public boolean isBooked() {
		return booked;
	}

	public void setBooked(boolean booked) {
		this.booked = booked;
	}
	
	public Integer getIdPK() {
		return idPK;
	}

	public void setIdPK(Integer idPK) {
		this.idPK = idPK;
	}
}
