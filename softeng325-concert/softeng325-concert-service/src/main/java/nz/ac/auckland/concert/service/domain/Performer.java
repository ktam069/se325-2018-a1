package nz.ac.auckland.concert.service.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import nz.ac.auckland.concert.common.types.Genre;

@Entity
@Table(name="Performers")
public class Performer {

	@Id
	@GeneratedValue
	@Column(nullable=false)
	private Long id;
	
	@Column(nullable=false)
	private String name;

	private String s3ImageUri;

	@Enumerated(EnumType.STRING)
	private Genre genre;

	@ManyToMany(mappedBy="performers", cascade=CascadeType.ALL)
	private Set<Concert> concerts;
	
	public Performer(Long id, String name, String s3ImageUri, Genre genre, Set<Concert> concerts) {
		this.id = id;
		this.name = name;
		this.s3ImageUri = s3ImageUri;
		this.genre = genre;
		this.concerts = concerts;
	}
	
	public Performer(String name, String s3ImageUri, Genre genre, Set<Concert> concerts) {
		this(null, name, s3ImageUri, genre, concerts);
	}
	
	// Required for JPA and JAXB.
	protected Performer() {}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageName() {
		return this.s3ImageUri;
	}

	public void setImageName(String s3ImageUri) {
		this.s3ImageUri = s3ImageUri;
	}

	public Genre getGenre() {
		return this.genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	
	public Set<Long> getConcertIds() {
		Set<Long> cIds = new HashSet<Long>();
		for (Concert c : concerts) {
			cIds.add(c.getId());
		}
		return cIds;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Performer, id: ");
		buffer.append(id);
		buffer.append(", name: ");
		buffer.append(name);
		buffer.append(", s3 image: ");
		buffer.append(s3ImageUri);
		buffer.append(", genre: ");
		buffer.append(genre.toString());
		
		return buffer.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Performer))
            return false;
        if (obj == this)
            return true;

        Performer rhs = (Performer) obj;
        return new EqualsBuilder().
            append(name, rhs.getName()).
            append(genre, rhs.getGenre()).
            append(s3ImageUri, rhs.getImageName()).
            isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). 
	            append(name).hashCode();
	}

}
