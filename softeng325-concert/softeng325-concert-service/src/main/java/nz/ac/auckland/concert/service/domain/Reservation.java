package nz.ac.auckland.concert.service.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.Collections;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import nz.ac.auckland.concert.common.dto.ReservationRequestDTO;
import nz.ac.auckland.concert.common.dto.SeatDTO;

/**
 * _id                 the unique identifier for a reservation.
 * _reservationRequest details of the corresponding reservation request, 
 *                     including the number of seats and their type, concert
 *                     identity, and the date/time of the concert for which a 
 *                     reservation was requested.
 * _seats              the seats that have been reserved (represented as a Set
 *                     of SeatDTO objects).
 */

@Entity
@Table(name="Reservations")
public class Reservation {

	@Id
	@GeneratedValue
	private Long id;
	
	private ReservationRequestDTO request;

	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="Res_Seats")
	private Set<Seat> seats;			// Note that Seat can be queried to find the concert and date
	
	private boolean confirmed;
	
	public Reservation() {}
	
	public Reservation(ReservationRequestDTO request, Set<Seat> seats) {
		this.request = request;
		this.seats = new HashSet<Seat>(seats);
		this.confirmed = false;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ReservationRequestDTO getRequest() {
		return request;
	}

	public void setRequest(ReservationRequestDTO request) {
		this.request = request;
	}

	public Set<Seat> getSeats() {
		return seats;
	}

	public void setSeats(Set<Seat> seats) {
		this.seats = seats;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
}