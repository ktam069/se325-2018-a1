package nz.ac.auckland.concert.service.mappers;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;

import nz.ac.auckland.concert.common.dto.PerformerDTO;
import nz.ac.auckland.concert.service.domain.Concert;
import nz.ac.auckland.concert.service.domain.Performer;
import nz.ac.auckland.concert.service.services.PersistenceManager;

public class PerformerMapper {
	
	public static PerformerDTO toDTO(Performer performer) {
		return new PerformerDTO(
				performer.getId(),
				performer.getName(),
				performer.getImageName(),
				performer.getGenre(),
				performer.getConcertIds());
	}
	
	public static Performer toDM(PerformerDTO performerDTO) {
		return new Performer(
				performerDTO.getName(),
				performerDTO.getImageName(),
				performerDTO.getGenre(),
				getConcerts( performerDTO.getConcertIds() ));
	}
	
	private static Set<Concert> getConcerts(Set<Long> cIds) {
		Set<Concert> concerts = new HashSet<Concert>();
		
		EntityManager em = PersistenceManager.instance().createEntityManager();
		try {
			em.getTransaction().begin();
			for (Long id : cIds) {
				Concert c = em.find(Concert.class, id);
				concerts.add(c);
			}
			em.getTransaction().commit();
			return concerts;
			
		} finally {
			em.close();
		}
	}
}
